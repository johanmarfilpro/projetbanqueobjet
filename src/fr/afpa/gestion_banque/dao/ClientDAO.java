package fr.afpa.gestion_banque.dao;

import java.util.Random;

import fr.afpa.gestion_banque.utils.TableauUtils;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.model.Client;

public class ClientDAO {

	
	private static Client[] clients = ClientDAO.generationStub(12);

	// private static String nextId() {
		// String chaineFormatte = String.format("%06d", cptClient++);
		// return "AB" + chaineFormatte;
	// }

	public static Client save(Client newClient) {
		if (newClient.getId() == 0) {
			newClient.setId(100000+ClientDAO.findAll().length+1);
		}
		ClientDAO.clients = TableauUtils.add(ClientDAO.clients, newClient);
		return newClient;
	}

	public static Client[] findAll() {
		return ClientDAO.clients;
	}

	public static Client findById(int id) {
		Client[] clients = findAll();
		for (Client currentClient : clients) {
			if (currentClient.getId() == id)
				return currentClient;
		}
		return null;
	}

	// public static String[][] findByNom(String nom) {
		// String[][] clients = findAll();
		// String[][] clientsResultats = new String[0][];
		// for (String[] currentClient : clients) {
			// if (currentClient[AppView.NOM_CLIENT].equalsIgnoreCase(nom))
				// clientsResultats = TableauUtils.add(clientsResultats, currentClient);
		// }
		// return clientsResultats;
	// }

	// public static String[] findByNumeroCompte(String idCompte) {
		// String[] compteBancaire = CompteBancaireDAO.findById(idCompte);
		// if (compteBancaire == null)
			// return null;

		// return findById(compteBancaire[AppView.CLIENT_ID_CB]);
	// }

	public static boolean exists(int id) {
		return findById(id) != null;
	}

	public static Client[] generationStub(int n) {
		Client[] clients = null;
		for (int i = 1; i <= n; i++) {
			clients = TableauUtils.add(clients,unClient(i));
		}
		return clients;
	}

	private static Client unClient(int i) {
		Random ran = new Random();
		String nomClient = "nom_client" + i;
		if(ran.nextBoolean())
			nomClient = "toto";
		
		Client currentClient = new Client(nomClient,"prenom_client" + i,"client" + i,"nom_client" + i + "@gmail.com");

		return currentClient;
	}

	public static boolean deleteById(int id) {
		Client[] clientsTmp = findAll();
		int indexClient = -1;
		for (int i = 0; i < clientsTmp.length; i++) {
			if (clientsTmp[i].getId()== id) {
				indexClient = i;
				break;
			}
		}
		if (indexClient < 0) {
			return false;
		}
		ClientDAO.clients = TableauUtils.supprimer(clientsTmp, indexClient);

		return true;
	}

	public static boolean update(Client newClient) {
		deleteById(newClient.getId());
		return save(newClient) != null;
	}

}
