package fr.afpa.gestion_banque.controller;

import fr.afpa.gestion_banque.dao.AgenceDAO;
// import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.view.AgenceView;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.model.*;

public class AgenceController {
	public static void creation() {
		
		Agence newAgence = AgenceView.affichageCreation();
		newAgence = AgenceDAO.save(newAgence);
		AppView.affichageMessage("L'agence " + newAgence.getCode() + " est créée");
	}

	public static void affichageAll() {
		Agence[] agences = AgenceDAO.findAll();
		AgenceView.affichageToutes(agences);
	}

	public static void suppression() {
		String codeAgence = AgenceView.affichageSuppression();
		if ("100".equals(codeAgence)) {
			AppView.affichageMessage("impossible de supprimer l'agence par defaut");
			return;
		}
		if (!AgenceDAO.exists(codeAgence)) {
			AppView.affichageMessage("L'agence " + codeAgence + " n'existe pas");
			return;
		}
		
		// String [][] comptesBancaires = CompteBancaireDAO.findAllByCodeAgence(codeAgence);
		
		// for (String[] currentCB : comptesBancaires) {
			// currentCB[AppView.AGENCE_CODE_CB]="0";
			// CompteBancaireDAO.update(currentCB);
		// }

		AgenceDAO.deleteByCode(codeAgence);
		
		AppView.affichageMessage("L'agence " + codeAgence + " est supprimee");
	}

	public static void modification() {
		String codeAgence = AgenceView.recuperationCodeAgence();
		if (codeAgence.equals("100")) {
			AppView.affichageMessage("Impossible de modifier l'agence par default");
			return;
		}
		
		Agence oldAgence = AgenceDAO.findByCode(codeAgence);
		
		if (oldAgence == null) {
			AppView.affichageMessage("L'agence " + codeAgence + " n'existe pas");
			return;
		}

		Agence newAgence = AgenceView.affichageModification(oldAgence);
		AgenceDAO.update(newAgence);
		AppView.affichageMessage("L'agence " + newAgence.getCode() + " est modifiée");

	}
}