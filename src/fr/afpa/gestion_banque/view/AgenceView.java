package fr.afpa.gestion_banque.view;

import fr.afpa.gestion_banque.model.*;


public class AgenceView {

	public static Agence affichageCreation() {
		
		System.out.println("Creation Agence : ");
		System.out.print("Nom : ");
		String nom = AppView.scanner.nextLine().trim();
		System.out.print("Adresse : ");
		String adresse = AppView.scanner.nextLine().trim();
		
		Agence newAgence = new Agence(nom, adresse);

		return newAgence;
	}

	public static void affichageToutes(Agence[] agences) {
		System.out.println("Liste agences : ");
		System.out.printf("%-15s\t%-15s\t%s\t", "CODE_AGENCE", "NOM_AGENCE", "ADRESSE_AGENCE");
		System.out.print(System.lineSeparator());
		
		
		for (int i = 0; i < agences.length; i++) {
			System.out.printf("%-15s\t%-15s\t%s\t", agences[i].getCode(), agences[i].getNom(), agences[i].getAdresse());
			System.out.print(System.lineSeparator());
		}
	}

	public static String affichageSuppression() {
		System.out.println("Suppression Agence : ");
		System.out.print("		Code agence : ");
		String codeAgence = AppView.scanner.nextLine().trim();
		return codeAgence;
	}

	public static Agence affichageModification(Agence oldAgence) {

		System.out.print("Nom (" + oldAgence.getCode() + "): ");
		String newNomAgence = AppView.scanner.nextLine().trim();
		if (!newNomAgence.isBlank() && !newNomAgence.isEmpty())
			oldAgence.setNom(newNomAgence);
		System.out.print("Adresse (" + oldAgence.getAdresse() + "): ");
		String newAdresseAgence = AppView.scanner.nextLine().trim();
		if (!newAdresseAgence.isBlank() && !newAdresseAgence.isEmpty())
			oldAgence.setAdresse(newAdresseAgence);

		return oldAgence;
	}

	public static String recuperationCodeAgence() {
		String codeAgence;
		System.out.println("Modification Agence : ");
		System.out.print("		Code agence : ");
		codeAgence = AppView.scanner.nextLine().trim();
		return codeAgence;
	}
}