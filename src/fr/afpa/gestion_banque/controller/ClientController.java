package fr.afpa.gestion_banque.controller;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.util.Date;

import fr.afpa.gestion_banque.dao.ClientDAO;
// import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.utils.TableauUtils;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.view.ClientView;
// import fr.afpa.gestion_banque.view.CompteBancaireView;
import fr.afpa.gestion_banque.model.Client;

public class ClientController {

	/*public static final String BASE_DIR = "C:/gestion_banque/impressions";
	public static final String BASE_FILE_NAME = "fiche_client";
	public static final String EXT = "txt";*/

	public static void creation() {
		Client newClient = ClientView.affichageCreation();
		newClient = ClientDAO.save(newClient);
		AppView.affichageMessage("Le client " + newClient.getId() + " est cr�e");
	}

	public static void affichageAll() {
		Client[] clients = ClientDAO.findAll();
		ClientView.affichageTous(clients);
	}

	public static void suppression() {
		int idClient = ClientView.affichageSuppression();
		Client currentClient = ClientDAO.findById(idClient);
		if (currentClient == null) {
			AppView.affichageMessage("Le client " + idClient + " n'existe pas");
			return;
		}

		/*String[][] comptesBancaires = CompteBancaireDAO.findAllByIdClient(idClient);
		for (String[] currentCB : comptesBancaires) {
			CompteBancaireDAO.deleteById(currentCB[AppView.ID_CB]);

		}*/
		ClientDAO.deleteById(idClient);
		AppView.affichageMessage("Le client " + idClient + " est supprime");
	}

	public static void modification() {
		int idClient = ClientView.recuperationIdClient();
		Client oldClient = ClientDAO.findById(idClient);
		if (oldClient == null) {
			AppView.affichageMessage("Le client " + idClient + " n'existe pas");
			return;
		}
		Client newClient = ClientView.affichageModification(oldClient);
		ClientDAO.update(newClient);
		AppView.affichageMessage("Le client " + newClient.getId() + " est modifie");

	}/*

	public static void chercherUnClient() {
		String choixCritere = ClientView.recuperationCritereRecherche();
		String[][] clients = new String[0][];

		clients = TableauUtils.add(clients, ClientDAO.findById(choixCritere));

		clients = TableauUtils.add(clients, ClientDAO.findByNom(choixCritere));

		clients = TableauUtils.add(clients, ClientDAO.findByNumeroCompte(choixCritere));

		ClientView.affichageTous(clients);

	}

	public static void impressionInfosClient() throws IOException {
		String idClient = ClientView.recuperationInfosImpression();

		String[] client = ClientDAO.findById(idClient);

		if (client == null) {
			AppView.affichageMessage("Le client " + idClient + " n'existe pas.");
			return;
		}

		String[][] compteBancaires = CompteBancaireDAO.findAllByIdClient(idClient);
		if (compteBancaires == null || compteBancaires.length == 0) {
			AppView.affichageMessage("Le client " + idClient + " ne possede pas de comptes bancaires");
			return;
		}
		Date now = new Date();
		String fileName = BASE_FILE_NAME + "_" + idClient + "_" + now.getTime() +"."+ EXT;
		Path cheminFile = Path.of(BASE_DIR, fileName);
		File file = cheminFile.toFile();

		if (!cheminFile.getParent().toFile().exists())
			cheminFile.getParent().toFile().mkdirs();

		if (!file.exists())
			file.createNewFile();

        PrintStream psMyFile = new PrintStream(file);
		CompteBancaireView.impressionInfosClient(client, compteBancaires, psMyFile);
		psMyFile.close();
	}*/

	}
