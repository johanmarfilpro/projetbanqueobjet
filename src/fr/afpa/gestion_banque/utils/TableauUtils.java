package fr.afpa.gestion_banque.utils;

import fr.afpa.gestion_banque.model.*;

import java.util.Random;
import java.util.Scanner;

public class TableauUtils {

	public static void afficher(Agence[] elements) {
		for (int i = 0; i < elements.length; i++) {
			System.out.print(elements[i]);
		}
		System.out.println();
	}

	public static void afficher(Client[] elements) {
		for (int i = 0; i < elements.length; i++) {
			System.out.print(elements[i]);
		}
		System.out.println();
	}

	public static void afficher(CompteBancaire[] elements) {
		for (int i = 0; i < elements.length; i++) {
			System.out.print(elements[i]);
		}
		System.out.println();
	}

	public static Agence[] redim(Agence[] tab) {
		Agence[] tmp = null;

		if (tab == null) {
			return tmp = new Agence[1];
		}

		tmp = new Agence[tab.length + 1];
		int tailleAParcourir = Math.min(tmp.length, tab.length);

		for (int i = 0; i < tailleAParcourir; i++) {
			tmp[i] = tab[i];
		}
		return tmp;
	}

	public static Client[] redim(Client[] tab) {
		Client[] tmp = null;
		if (tab == null) {
			return tmp = new Client[1];
		}
		tmp = new Client[tab.length + 1];

		int tailleAParcourir = Math.min(tmp.length, tab.length);

		for (int i = 0; i < tailleAParcourir; i++) {
			tmp[i] = tab[i];
		}
		return tmp;
	}

	public static CompteBancaire[] redim(CompteBancaire[] tab) {
		CompteBancaire[] tmp = null;
		if (tab == null) {
			return tmp = new CompteBancaire[1];
		}
		tmp = new CompteBancaire[tab.length + 1];
		
		
		int tailleAParcourir = Math.min(tmp.length, tab.length);

		for (int i = 0; i < tailleAParcourir; i++) {
			tmp[i] = tab[i];
		}
		return tmp;
	}

	/**
	 * Cette methode permet d'ajouter un element � la fin du tableau @param tab
	 * 
	 * @return int[]
	 */
	public static Agence[] add(Agence[] tab, Agence element) {
		tab = TableauUtils.redim(tab);
		tab[tab.length - 1] = element;
		return tab;
	}

	public static Client[] add(Client[] tab, Client element) {
		tab = TableauUtils.redim(tab);
		tab[tab.length - 1] = element;
		return tab;
	}

	public static CompteBancaire[] add(CompteBancaire[] tab, CompteBancaire element) {
		tab = TableauUtils.redim(tab);
		tab[tab.length - 1] = element;
		return tab;
	}
	
	public static CompteBancaire[] add(CompteBancaire[] tab, CompteBancaire[] elements) {
		
		for (int i = 0; i < elements.length; i++) {
			tab = add(tab, elements[i]);
		}
		
		return tab;
	}

	public static Agence[] supprimer(Agence[] tableau, int index) {

		Agence tmp[] = null;

		if (tableau != null && tableau.length > 0)
			tmp = new Agence[tableau.length - 1];

		if (tableau == null)
			throw new IllegalArgumentException("Tableau en entr�e est null");
		if (tableau.length == 0)
			throw new IllegalArgumentException("Tableau est vide");
		if (index < 0 || index > tmp.length)
			throw new IllegalArgumentException("L'index est incorrect");

		for (int i = 0; i < tmp.length; i++) {
			if (i < index) {
				tmp[i] = tableau[i];
			} else {
				tmp[i] = tableau[i + 1];
			}
		}

		return tmp;
	}

	public static CompteBancaire[] supprimer(CompteBancaire[] tableau, int index) {

		CompteBancaire tmp[] = null;

		if (tableau != null && tableau.length > 0)
			tmp = new CompteBancaire[tableau.length - 1];

		if (tableau == null)
			throw new IllegalArgumentException("Tableau en entr�e est null");
		if (tableau.length == 0)
			throw new IllegalArgumentException("Tableau est vide");
		if (index < 0 || index > tmp.length)
			throw new IllegalArgumentException("L'index est incorrect");

		for (int i = 0; i < tmp.length; i++) {
			if (i < index) {
				tmp[i] = tableau[i];
			} else {
				tmp[i] = tableau[i + 1];
			}
		}

		return tmp;
	}

	public static Client[] supprimer(Client[] tableau, int index) {

		Client tmp[] = null;

		if (tableau != null && tableau.length > 0)
			tmp = new Client[tableau.length - 1];

		if (tableau == null)
			throw new IllegalArgumentException("Tableau en entr�e est null");
		if (tableau.length == 0)
			throw new IllegalArgumentException("Tableau est vide");
		if (index < 0 || index > tmp.length)
			throw new IllegalArgumentException("L'index est incorrect");

		for (int i = 0; i < tmp.length; i++) {
			if (i < index) {
				tmp[i] = tableau[i];
			} else {
				tmp[i] = tableau[i + 1];
			}
		}

		return tmp;
	}

}