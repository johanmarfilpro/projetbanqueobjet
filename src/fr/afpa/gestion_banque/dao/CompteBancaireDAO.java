package fr.afpa.gestion_banque.dao;

import java.util.Random;

import fr.afpa.gestion_banque.model.CompteBancaire;
import fr.afpa.gestion_banque.utils.TableauUtils;
import fr.afpa.gestion_banque.view.AppView;

// import java.util.Random;

// import fr.afpa.gestion_banque.utils.TableauUtils;
// import fr.afpa.gestion_banque.view.AppView;

public class CompteBancaireDAO {

	private static CompteBancaire[] compteBancaires = CompteBancaireDAO.generationStub(7);

	// private static String nextId() {
		// return "" + (cptCompteBancaire++);
	// }

	public static CompteBancaire save(CompteBancaire newCompteBancaire) {
		CompteBancaireDAO.compteBancaires = TableauUtils.add(CompteBancaireDAO.compteBancaires, newCompteBancaire);
		return newCompteBancaire;
	}

	public static CompteBancaire[] findAll() {
		return CompteBancaireDAO.compteBancaires;
	}

	public static CompteBancaire findById(int id) {
		CompteBancaire[] compteBancaires = findAll();
		for (CompteBancaire currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire.getId() == id)
				return currentCompteBancaire;
		}
		return null;
	}

	// public static String[][] findAllByCodeAgence(String codeAgence) {
		// String[][] compteBancaires = findAll();
		// String[][] resultatsCompteBancaires = new String[0][];
		// for (String[] currentCompteBancaire : compteBancaires) {
			// if (currentCompteBancaire[AppView.AGENCE_CODE_CB].equalsIgnoreCase(codeAgence)) {
				// resultatsCompteBancaires = TableauUtils.add(resultatsCompteBancaires, currentCompteBancaire);
			// }
		// }
		// return resultatsCompteBancaires;
	// }
	
	public static CompteBancaire[] findAllByIdClient(String idClient) {
		CompteBancaire[] compteBancaires = findAll();
		CompteBancaire[] resultatsCompteBancaires = new CompteBancaire[0];
		for (CompteBancaire currentCompteBancaire : compteBancaires) {
			if (currentCompteBancaire.getCodeClient() == Integer.parseInt(idClient)) {
				resultatsCompteBancaires = TableauUtils.add(resultatsCompteBancaires, currentCompteBancaire);
			}
		}
		return resultatsCompteBancaires;
	}

	public static int countClientCompteBancaire(String idClient) {
		CompteBancaire[] compteBancaires = findAllByIdClient(idClient);
		return compteBancaires.length;
	}

	public static boolean aPlusQue3CompteComptes(String idClient) {
		return countClientCompteBancaire(idClient) >= 3;
	}

	public static boolean aUnCompteBancaireDuType(String idClient, String type) {
		CompteBancaire[] compteBancaires = findAllByIdClient(idClient);
		for (CompteBancaire currentCompte : compteBancaires) {
			if (currentCompte.getType().equalsIgnoreCase(type))
				return true;
		}

		return false;
	}

	public static CompteBancaire[] generationStub(int nombreClient) {
		CompteBancaire[] compteBancaires = null;
		int idClient;
		for (int i = 1; i <= nombreClient; i++) {
			idClient = 100000+i;
			compteBancaires = TableauUtils.add(compteBancaires, plusieursCb(idClient));
		}
		return compteBancaires;
	}

	private static CompteBancaire[] plusieursCb(int idClient) {
		CompteBancaire[] comptesbancaires = null;
		Random ran = new Random();
		int nombreComptesBancaires = ran.nextInt(3 + 1 - 1) + 1;

		if (nombreComptesBancaires >= 1) {
			comptesbancaires = TableauUtils.add(comptesbancaires, unCB(idClient, AppView.COMPTE_COURANT));
		}
		if (nombreComptesBancaires >= 2) {
			comptesbancaires = TableauUtils.add(comptesbancaires, unCB(idClient, AppView.LIVRET_A));
		}
		if (nombreComptesBancaires >= 3) {
			comptesbancaires = TableauUtils.add(comptesbancaires, unCB(idClient, AppView.PLAN_EPARGNE_LOGEMENT));
		}

		return comptesbancaires;
	}

	private static CompteBancaire unCB(int idClient, String typeCompte) {
		Random ran = new Random();
		int codeAgence = ran.nextInt(10+1-1)+1;
		CompteBancaire newCompteBancaire = new CompteBancaire( Math.round(ran.nextDouble()*100001) , ran.nextBoolean(), typeCompte, codeAgence, idClient);

		return newCompteBancaire;
	}

	public static boolean exists(int id) {
		return findById(id)!=null;
	}

	
	public static boolean deleteById(int id) {
		CompteBancaire[] compteBancairesTmp = findAll();
		int indexCompteBancaire = -1;
		for (int i = 0; i < compteBancairesTmp.length; i++) {
			if (compteBancairesTmp[i].getId() == id) {
				indexCompteBancaire = i;
				break;
			}
		}
		if (indexCompteBancaire < 0) {
			return false;
		}
		CompteBancaireDAO.compteBancaires = TableauUtils.supprimer(compteBancairesTmp, indexCompteBancaire);

		return true;
	}

	public static boolean update(CompteBancaire newCompteBancaire) {
		deleteById(newCompteBancaire.getId());
		return save(newCompteBancaire) != null;
	}

}
