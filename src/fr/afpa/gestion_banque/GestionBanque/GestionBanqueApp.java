package fr.afpa.gestion_banque.GestionBanque;

import java.io.IOException;

import fr.afpa.gestion_banque.controller.AgenceController;
// import fr.afpa.gestion_banque.controller.AgenceController;
import fr.afpa.gestion_banque.controller.ClientController;
import fr.afpa.gestion_banque.controller.CompteBancaireController;
// import fr.afpa.gestion_banque.controller.CompteBancaireController;
import fr.afpa.gestion_banque.view.AppView;

public class GestionBanqueApp {

	public static void main(String[] args) throws IOException {

		do {
			String choixUtilisateur = AppView.affichageMenu();

			if (choixUtilisateur.equalsIgnoreCase("1")) {
				AgenceController.creation();
			} else if (choixUtilisateur.equalsIgnoreCase("2")) {
				ClientController.creation();
			} else if (choixUtilisateur.equalsIgnoreCase("3")) {
				CompteBancaireController.creation();
			} else if (choixUtilisateur.equalsIgnoreCase("4")) {
				AgenceController.modification();
			} else if (choixUtilisateur.equalsIgnoreCase("5")) {
				ClientController.modification();
			} else if (choixUtilisateur.equalsIgnoreCase("6")) {
				CompteBancaireController.modification();
			} else if (choixUtilisateur.equalsIgnoreCase("7")) {
				AgenceController.suppression();
			} else if (choixUtilisateur.equalsIgnoreCase("8")) {
				ClientController.suppression();
			} else if (choixUtilisateur.equalsIgnoreCase("9")) {
				CompteBancaireController.suppression();
			} else if (choixUtilisateur.equalsIgnoreCase("10")) {
				AgenceController.affichageAll();
			} else if (choixUtilisateur.equalsIgnoreCase("11")) {
				ClientController.affichageAll();
			} else if (choixUtilisateur.equalsIgnoreCase("12")) {
				CompteBancaireController.affichageAll();
			} else if (choixUtilisateur.equalsIgnoreCase("13")) {
				//CompteBancaireController.chercherUnCompteBancaire();
			} else if (choixUtilisateur.equalsIgnoreCase("14")) {
				//ClientController.chercherUnClient();
			} else if (choixUtilisateur.equalsIgnoreCase("15")) {
				//CompteBancaireController.chercherTousCompteBancaireClient();
			} else if (choixUtilisateur.equalsIgnoreCase("16")) {
				
				//ClientController.impressionInfosClient();
			} else if (choixUtilisateur.equalsIgnoreCase("17")) {
				System.out.println("Au revoir");
				break;
			} else {
				System.out.println("Choix invalide");
			}
			System.out.println("Continuez ...");
			AppView.scanner.nextLine();
		} while (true);
	}

}
