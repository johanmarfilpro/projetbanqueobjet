package fr.afpa.gestion_banque.view;

//import fr.afpa.gestion_banque.dao.ClientDAO;
import fr.afpa.gestion_banque.model.Client;

public class ClientView {

	public static Client affichageCreation() {
		System.out.println("Creation Client : ");
		System.out.print("Nom : ");
		String nom = AppView.scanner.nextLine().trim();
		System.out.print("Prenom : ");
		String prenom = AppView.scanner.nextLine().trim();
		System.out.print("Date naissance (dd-MM-aaaa): ");
		String dateNaissance = AppView.scanner.nextLine().trim();
		System.out.print("Mail : ");
		String mail = AppView.scanner.nextLine().trim();
		
		Client newClient = new Client(nom,prenom,dateNaissance,mail);

		return newClient;
	}

	public static void affichageTous(Client[] clients) {
		System.out.println("Liste clients : ");
		System.out.println("ID        NOM               PRENOM                DATE_NAISSANCE               MAIL");
		System.out.print(System.lineSeparator());
		for (int i = 0; i < clients.length; i++) {
			Client currentClient = clients[i];
			System.out.println(currentClient);
		}
		// /*
		 // * for (String[] currentClient : clients) {
		 // * System.out.printf("%10s\t%10s\t%10s\t",currentClient[0],currentClient[1],
		 // * currentClient[2]); System.out.print(System.lineSeparator()); }
		 // */
	}

	// public static void main(String[] args) {
		// ClientView.affichageTous(ClientDAO.findAll());
	// }

	public static int affichageSuppression() {
		System.out.println("Suppression Client : ");
		System.out.print("		Id client : ");
		int idClient = Integer.parseInt(AppView.scanner.nextLine().trim());
		return idClient;
	}

	public static Client affichageModification(Client oldClient) {

		System.out.print("Nom (" + oldClient.getNom() + "): ");
		String newNomClient = AppView.scanner.nextLine().trim();
		if (!newNomClient.isBlank() && !newNomClient.isEmpty())
			oldClient.setNom(newNomClient);

		System.out.print("Prenom (" + oldClient.getPrenom() + "): ");
		String newPrenomClient = AppView.scanner.nextLine().trim();
		if (!newPrenomClient.isBlank() && !newPrenomClient.isEmpty())
			oldClient.setPrenom(newPrenomClient);

		System.out.print("Date de naissance (" + oldClient.getDateNaissance() + "): ");
		String newDateClient = AppView.scanner.nextLine().trim();
		if (!newDateClient.isBlank() && !newDateClient.isEmpty())
			oldClient.setDateNaissance(newDateClient);

		System.out.print("Mail (" + oldClient.getMail() + "): ");
		String newMailClient = AppView.scanner.nextLine().trim();
		if (!newMailClient.isBlank() && !newMailClient.isEmpty())
			oldClient.setMail(newMailClient);
		return oldClient;
	}

	public static int recuperationIdClient() {
		int idClient;
		System.out.println("Modification Client : ");
		System.out.print("		ID client : ");
		idClient = Integer.parseInt(AppView.scanner.nextLine().trim());
		return idClient;
	}
	
	// public static String recuperationCritereRecherche() {
		// String idClient;
		// System.out.println("Recherche de client (Nom, Num�ro de compte, identifiant de client) : ");
		// System.out.print("		Choix : ");
		// idClient = AppView.scanner.nextLine().trim();
		// return idClient;
	// }

	// public static String recuperationInfosImpression() {
		
		// String idClient;
		// System.out.println("Imprimer les infos client (identifiant client) : ");
		// System.out.print("		ID client : ");
		// idClient = AppView.scanner.nextLine().trim();
		// return idClient;
	// }
}
