package fr.afpa.gestion_banque.model;

public class Client{
	private static int cpt;
	private int id;
	private String nom;
	private String prenom;
	private String dateNaissance;
	private String mail;
	
	static{
		cpt = 100001;
	}
	
	
	public Client(){
		
	}
	
	public Client(String nom, String prenom, String dateNaissance, String mail){
		this.id = cpt++;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaissance = dateNaissance ;
		this.mail = mail;
	}

	public int getId(){
		return this.id;
	}
	public void setId(int id){
		this.id = id;
	}
	
	public String getNom(){
		return this.nom;
	}
	
	public void setNom(String nom){
		this.nom = nom;
	}
	
	public String getPrenom(){
		return this.prenom;
	}
	
	public void setPrenom(String prenom){
		this.prenom = prenom;
	}
	
	public void setDateNaissance(String dateNaissance){
		this.dateNaissance = dateNaissance;
	}
	
	public String getDateNaissance(){
		return this.dateNaissance;
	}

	public String getMail(){
		return this.mail;
	}
	
	public void setMail(String mail){
		this.mail = mail;
	}
	
	public String toString() {
		return "AB"+this.id+"        "+this.nom+"               "+this.prenom+"                "+this.dateNaissance+"               "+this.mail;
	}
	
}