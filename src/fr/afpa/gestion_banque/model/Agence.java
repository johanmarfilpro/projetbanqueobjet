package fr.afpa.gestion_banque.model;

public class Agence{
	private static int cpt;
	private int code;
	private String nom = "Generique";
	private String adresse = "Inconnu";

	static{
		cpt = 100;
	}
	
	public Agence(){
		
	}
	
	public Agence(String nom, String adresse){
		this.code = cpt++;
		this.nom = nom;
		this.adresse = adresse;
	}
	

	
	public int getCode(){
		return this.code;
	}
	
	public void setCode(int code){
		this.code = code;
	}
	
	public String getNom(){
		return this.nom;
	}
	
	public void setNom(String nom){
		this.nom = nom;
	}
	
	public String getAdresse(){
		return this.adresse;
	}
	
	public void setAdresse(String adresse){
		this.adresse = adresse;
	}
	
	public String toString(){
		return "" + this.code;
	}
}