package fr.afpa.gestion_banque.dao;

import fr.afpa.gestion_banque.utils.TableauUtils;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.model.*;

public class AgenceDAO {

	private static Agence[] agences = generationStub(12);
	
	
	public static Agence save(Agence newAgence) {
		if(newAgence.getCode() == 0){
			newAgence.setCode(100+AgenceDAO.findAll().length+1);
		}
		AgenceDAO.agences = TableauUtils.add(AgenceDAO.agences, newAgence);
		return newAgence;
	}

	public static Agence[] findAll() {
		return AgenceDAO.agences;
	}
	
	public static Agence findByCode(String codeAgence){
		Agence[] agences = findAll();
		for(int i=0; i<agences.length; i++){
			if(("" + agences[i].getCode()).equalsIgnoreCase(codeAgence)){
				return agences[i];
			}
		}
		return null;
	}
	
	

	public static Agence[] generationStub(int n) {
		Agence[] agences = new Agence[n];

		agences[0] = new Agence("Agence generique","INCONNUE");
		
		for (int i = 2; i <= agences.length; i++) {
			agences[i-1] = new Agence("agence" + i, "adresse" + i);
		}

		return agences;
	}
	
	public static boolean exists(String code) {
		return findByCode(code)!=null;
	}
	
	public static boolean deleteByCode(String codeAgence) {
		Agence[] agencesTmp = findAll();
		int indexAgence = -1;
		for (int i = 0; i < agencesTmp.length; i++) {
			if ((""+agencesTmp[i].getCode()).equalsIgnoreCase(codeAgence)) {
				indexAgence = i;
				break;
			}
		}
		if (indexAgence < 0) {
			return false;
		}
		AgenceDAO.agences = TableauUtils.supprimer(agencesTmp, indexAgence);

		return true;
	}

	public static boolean update(Agence newAgence) {
		deleteByCode("" + newAgence.getCode());
		return save(newAgence) != null;
	}

}