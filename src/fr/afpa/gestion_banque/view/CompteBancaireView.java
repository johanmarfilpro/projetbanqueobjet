package fr.afpa.gestion_banque.view;

import fr.afpa.gestion_banque.model.CompteBancaire;

// import java.io.PrintStream;

public class CompteBancaireView {

	public static CompteBancaire affichageCreation() {
		System.out.println("Creation CompteBancaire : ");
		System.out.print("Solde : ");
		double solde = Double.parseDouble(AppView.scanner.nextLine().trim());
		System.out.print("Decouvert : ");
		System.out.print("inserer : true/false");
		String dec = AppView.scanner.nextLine().trim();
		boolean decouvert = Boolean.parseBoolean(dec);
		System.out.print("Type (CC, LA, PEL) : ");
		String type = AppView.scanner.nextLine().trim();
		System.out.print("Id client : ");
		int idClient = Integer.parseInt(AppView.scanner.nextLine().trim());
		System.out.print("Code Agence : ");
		int codeAgence = Integer.parseInt(AppView.scanner.nextLine().trim());
		
		CompteBancaire newCompteBancaire = new CompteBancaire(solde, decouvert, type, codeAgence, idClient);

		return newCompteBancaire;
	}

	public static void affichageTous(CompteBancaire[] compteBancaires) {

		System.out.println("Liste compteBancaires : ");
		System.out.printf("%-11s\t%-15s\t%-15s\t%-18s\t%-18s\t%s", "ID_COMPTE", "SOLDE", "DECOUVERT", "TYPE",
				"ID_CLIENT", "CODE_AGENCE");
		System.out.print(System.lineSeparator());
		for (int i = 0; i < compteBancaires.length; i++) {
			System.out.printf("%-11s\t%-15s\t%-15s\t%-18s\t%-18s\t%s", compteBancaires[i].getId(),
					compteBancaires[i].getSolde(),
					compteBancaires[i].getDecouvert(),
					compteBancaires[i].getType(),
					compteBancaires[i].getCodeClient(),
					compteBancaires[i].getCodeAgence());
			System.out.print(System.lineSeparator());
		}

	}

	// private static void affichageBasique(String[] currentCompteBancaire) {
		// System.out.printf("%011d\t%-15s\t%-15s\t%-18s\t%-18s\t%03d",
				// Integer.parseInt(currentCompteBancaire[AppView.ID_CB]), currentCompteBancaire[AppView.SOLDE_CB],
				// currentCompteBancaire[AppView.DECOUVERT_CB],
				// AppView.getTypeCompteNom(currentCompteBancaire[AppView.TYPE_CB]),
				// currentCompteBancaire[AppView.CLIENT_ID_CB],
				// Integer.parseInt(currentCompteBancaire[AppView.AGENCE_CODE_CB]));
		// System.out.print(System.lineSeparator());
	// }

	public static int affichageSuppression() {
		System.out.println("Suppression CompteBancaire : ");
		System.out.print("		Id compteBancaire : ");
		int idCompteBancaire = Integer.parseInt(AppView.scanner.nextLine().trim());
		return idCompteBancaire;
	}

	public static CompteBancaire affichageModification(CompteBancaire oldCompteBancaire) {

		System.out.print("Solde (" + oldCompteBancaire.getSolde() + "): ");
		String newSoldeCompteBancaire =  AppView.scanner.nextLine().trim();
		if (!newSoldeCompteBancaire.isBlank() && !newSoldeCompteBancaire.isEmpty())
			oldCompteBancaire.setSolde(Double.parseDouble(newSoldeCompteBancaire));

		System.out.print("Decouvert (" + oldCompteBancaire.getDecouvert() + "): ");
		String newDecouvertCompteBancaire = AppView.scanner.nextLine().trim();
		if (!newDecouvertCompteBancaire.isBlank() && !newDecouvertCompteBancaire.isEmpty())
			oldCompteBancaire.setDecouvert(Boolean.parseBoolean(newDecouvertCompteBancaire));

		return oldCompteBancaire;
	}

	public static int recuperationIdCompteBancaire(String message) {
		int idCompteBancaire;
		System.out.println(message);
		System.out.print("		ID compteBancaire : ");
		idCompteBancaire = Integer.parseInt( AppView.scanner.nextLine().trim());
		return idCompteBancaire;
	}

	// public static void impressionInfosClient(String[] client, String[][] compteBancaires,PrintStream myOut) {
		
		// myOut.printf("%20s%10s%-30s%20s","","","Fiche client","");
		// myOut.println();
		// myOut.println("Numero client : "+client[AppView.ID_CLIENT]);
		// myOut.println("Nom : "+client[AppView.NOM_CLIENT]);
		// myOut.println("Prenom : "+client[AppView.PRENOM_CLIENT]);
		// myOut.println("Date de naissance : "+client[AppView.DATE_NAISSANCE_CLIENT]);
		// myOut.println();
		// myOut.println();
		// myOut.println("_".repeat(80));  
		// myOut.println();
		// myOut.printf("%20s%10s%-30s%20s","","","Liste de compte","");
		// myOut.println();
		// myOut.println("_".repeat(80)); 
		// myOut.println();
		// myOut.printf("%-28s%-25s","Num�ro de compte","Solde");
		// myOut.println();
		// myOut.println("_".repeat(80));
		// myOut.println();
		// String numero = null;
		// String solde = null;
		// String icone = null;
		
		// for (String[] currentCompteBancaires : compteBancaires) {
			// numero = String.format("%011d", Integer.parseInt(currentCompteBancaires[AppView.ID_CB]));
			// solde = currentCompteBancaires[AppView.SOLDE_CB]+ " euros";
			// icone = Integer.parseInt(currentCompteBancaires[AppView.SOLDE_CB])>=0?":-)":":-(";
			// myOut.printf("%-28s%-25s%s",numero,solde,icone);
			// myOut.println();
		// }
	
		
	// }




}
