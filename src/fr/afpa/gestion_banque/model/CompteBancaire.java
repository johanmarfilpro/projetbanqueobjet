package fr.afpa.gestion_banque.model;

public class CompteBancaire{
	private static int cpt;
	private int id;
	private double solde;
	private boolean decouvert;
	private String type;
	private int codeAgence;
	private int codeClient;
	
	static {
		cpt = 1000000000;
	}
	
	public CompteBancaire(){
		
	}
	
	public CompteBancaire(double solde, boolean decouvert, String type,int codeAgence,int codeClient){
		this.id = cpt++;
		this.solde = solde;
		this.decouvert = decouvert;
		this.type = type ;
		this.codeAgence = codeAgence;
		this.codeClient = codeClient;
	}
	
	public int getId(){
		return this.id;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public double getSolde(){
		return this.solde;
	}
	
	public void setSolde(double solde){
		this.solde = solde;
	}
	
	public boolean getDecouvert(){
		return this.decouvert;
	}
	
	public void setDecouvert(boolean decouvert){
		this.decouvert = decouvert;
	}
	//
	public String getType(){
		return this.type;
	}
	
	public void setType(String type){
		this.type = type;
	}
	
	public int getCodeAgence(){
		return this.codeAgence;
	}
	
	public int getCodeClient(){
		return this.codeClient;
	}
}