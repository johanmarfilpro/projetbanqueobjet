package fr.afpa.gestion_banque.view.validator;

import fr.afpa.gestion_banque.dao.AgenceDAO;
import fr.afpa.gestion_banque.dao.ClientDAO;
import fr.afpa.gestion_banque.dao.CompteBancaireDAO;
import fr.afpa.gestion_banque.view.AppView;
import fr.afpa.gestion_banque.model.CompteBancaire;
import fr.afpa.gestion_banque.model.Client;
import fr.afpa.gestion_banque.model.Agence;

public class CompteBancairesValidator {
	public static boolean verificationRGs(CompteBancaire newCompteBancaire) {
		Client client = ClientDAO.findById(newCompteBancaire.getCodeClient());
		if (client == null) {
			System.out.println("Le client " + newCompteBancaire.getCodeClient() + " n'existe pas");
			return false;
		}

		Agence agence = AgenceDAO.findByCode("" + newCompteBancaire.getCodeAgence());
		if (agence == null) {
			System.out.println("L'agence " + newCompteBancaire.getCodeAgence() + " n'existe pas");
			return false;
		}

		if (!newCompteBancaire.getType().equalsIgnoreCase(AppView.COMPTE_COURANT)
				&& !newCompteBancaire.getType().equalsIgnoreCase(AppView.LIVRET_A)
				&& !newCompteBancaire.getType().equalsIgnoreCase(AppView.PLAN_EPARGNE_LOGEMENT)) {
			System.out.println("Le type " + newCompteBancaire.getType() + " n'existe pas");
			return false;
		}

		if (CompteBancaireDAO.aPlusQue3CompteComptes("" + newCompteBancaire.getCodeClient())) {
			System.out.println("Le client " + newCompteBancaire.getCodeClient() + " a deja 3 comptes");
			return false;
		}

		if (CompteBancaireDAO.aUnCompteBancaireDuType("" + newCompteBancaire.getCodeClient(),
				"" + newCompteBancaire.getType())) {
			System.out.println("Le client " + newCompteBancaire.getCodeClient() + " a deja un comptedu type <"
					+ newCompteBancaire.getType() + ">");
			return false;
		}

		return true;
	}
}
